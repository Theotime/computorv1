<?php

class Operation {

	private $n;
	private $p;

	public function __construct($n, $p) {
		$this->n = $n;
		$this->p = $p;
	}

	public function getN() {
		return $this->n;
	}

	public function getP() {
		return $this->p;
	}

	public function __toString() {
		$str = '';
		if ($this->n != 1 || $this->p == 0)
			$str .= $this->n;
		if ($this->p != 0 && $this->p != 1)
			$str .= 'x^'. $this->p;
		else if ($this->p == 1 && $this->n != 0)
			$str .= 'x';
		return $str;
	}

}