<?php

require_once __DIR__. "/Operation.class.php";

class Calcule {

	private $op;
	private $n = 0;

	public function __construct($str) {
		$this->op = $this->parse($str);
		$len = count($this->op);
		$this->reduce();
	}

	public function getMaxPower() {
		$n = false;

		foreach ($this->op as $op) {
			if (!$op instanceof Operation)
				continue ;
			if ($op->getP() > $n || $n === false) {
				$n = $op->getP();
			}
		}
		return $n;
	}

	public function getMinPower() {
		$n = false;

		foreach ($this->op as $op) {
			if (!$op instanceof Operation)
				continue ;
			if ($op->getP() < $n || $n === false) {
				$n = $op->getP();
			}
		}
		return $n;
	}

	public function getFirst() {
		$tmp = $this->op[0];
		if ($this->n > 1) {
			if ($this->op[1] == '-')
				$this->op[2] = $this->mult($this->op[2], new Operation(-1, 0));
			array_splice($this->op, 0, 2);
		} else {
			array_splice($this->op, 0, 1);
		}
		--$this->n;
		return $tmp;
	}

	public function insert(Operation $op) {
		if (!$this->matchAdd($op, -1)) {
			if ($this->n > 0) {
				if ($op->getN() < 0) {
					$this->op[] = '-';
					$this->op[] = $this->mult($op, new Operation(-1, 0));
				} else {
					$this->op[] = '+';
					$this->op[] = $op;
				}
			} else {
				$this->op[] = $op;
			}
		}
		++$this->n;
	}

	public function mult(Operation $op1, Operation $op2) {
		$n = $op1->getN() * $op2->getN();
		$p = $op1->getP() + $op2->getP();
		return new Operation($n, $p);
	}

	public function div(Operation $op1, Operation $op2) {
		$n = $op1->getN() / $op2->getN();
		$p = $op1->getP() - $op2->getP();
		return new Operation($n, $p);
	}

	public function add(Operation $op1, Operation $op2) {
		$n = $op1->getN() + $op2->getN();
		$p = $op1->getP();
		return new Operation($n, $p);
	}

	public function sub(Operation $op1, Operation $op2) {
		$n = $op1->getN() - $op2->getN();
		$p = $op1->getP();
		return new Operation($n, $p);
	}

	public function getN() {
		return $this->n;
	}

	public function __toString()
	{
		$b = true;
		$str = '';
		foreach ($this->op as $op)
		{
			if (!$b)
				$str .= ' ';
			$str .= $op;
			$b = false;
		}
		return $str;
	}

	private function reduce() {
		for ($i = 0; $i < count($this->op); ++$i) {
			if ($this->op[$i] != '*' && $this->op[$i] != '/')
				continue ;
			if ($this->op[$i] == '*') {
				$tmp = $this->mult($this->op[$i - 1], $this->op[$i + 1]);
				array_splice($this->op, $i - 1, 3, [$tmp]);
			} else if ($this->op[$i] == '/') {
				$tmp = $this->div($this->op[$i - 1], $this->op[$i + 1]);
				array_splice($this->op, $i - 1, 3, [$tmp]);
			}
			--$this->n;
			--$i;
		}
		$this->cleanZeroValue();

		for ($i = 0; $i < count($this->op); ++$i) {
			if ($this->op[$i] == '+') {
				$tmp = $this->op[$i + 1];
				if ($this->matchAdd($tmp, $i + 1)) {
					array_splice($this->op, $i, 2);
					--$this->n;
					--$i;
				}
			}
			if ($this->op[$i] == '-') {
				$tmp = $this->op[$i + 1];
				if ($this->matchSub($tmp, $i + 1)) {
					array_splice($this->op, $i, 2);
					--$this->n;
					--$i;
				}
			}
		}
		$this->cleanZeroValue();
	}

	private function matchAdd(Operation $op, $n)
	{
		for ($i = 0; $i < count($this->op); ++$i) {
			if ($i == $n)
				continue ;
			if ($this->op[$i] instanceof Operation && $this->op[$i]->getP() == $op->getP()) {
				if ($i > 0 && $this->op[$i - 1] == '-')
					$this->op[$i] = $this->add($this->op[$i], $this->mult(new Operation(-1, 0), $op));
				else
					$this->op[$i] = $this->add($this->op[$i], $op);
				return true;
			}
		}
		return false;
	}

	private function matchSub(Operation $op, $n)
	{
		for ($i = 0; $i < count($this->op); ++$i) {
			if ($i == $n)
				continue ;
			if ($this->op[$i] instanceof Operation && $this->op[$i]->getP() == $op->getP()) {
				if ($i > 0 && $this->op[$i - 1] == '-')
					$this->op[$i] = $this->sub($this->op[$i], $this->mult(new Operation(-1, 0), $op));
				else
					$this->op[$i] = $this->sub($this->op[$i], $op);
				return true;
			}
		}
		return false;
	}

	private function cleanZeroValue() {
		if (count($this->op) < 2)
			return ;
		for ($i = 0; $i < count($this->op); ++$i) {
			if (!($this->op[$i] instanceof Operation) || $this->op[$i]->getN())
				continue ;
			if ($i != 0) {
				array_splice($this->op, $i - 1, 2);
			} else {
				if (isset($this->op[$i + 1]) && $this->op[$i + 1] == '-')
					$this->op[$i + 2] = $this->mult($this->op[$i + 2], new Operation(-1, 0));
				array_splice($this->op, $i, 2);
			}
			--$this->n;
			--$i;
		}
	}

	private function parse($str) {
		$ops = split(' ', $str);
		$len = count($ops);
		$op = [];

		for ($i = 0; $i < $len; ++$i)
		{
			$n = 0;
			$p = 0;
			if (preg_match('/^-?\d+(\.\d+)?$/', $ops[$i])) {
				$n = floatval($ops[$i]);
			} else if (preg_match('/^[xX]$/', $ops[$i])) {
				$n = 1;
				$p = 1;
			} else if (preg_match('/^-[xX](\^(\d+)?)?$/', $ops[$i], $match)) {
				$n = -1;
				$p = isset($match[2]) ? $match[2] : 1;
			} else if (preg_match('/^(-?\d+(\.\d+)?)?[Xx](\^(\d+)?)?$/', $ops[$i], $match)) {
				$n = empty($match[1]) && $match[1] != '0' ? 1 : floatval($match[1]);
				$p = isset($match[4]) ? $match[4] : 1;
			} else {
				$op[] = $ops[$i];
				continue ;
			}
			$o = new Operation($n, $p);
			$op[] = $o;
			++$this->n;
		}
		return $op;
	}

}