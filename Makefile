all: unit

unit: good bad

bonus:
	./main.php "5 + 4X = 4"

# 
# UNIT TEST GOOD
# 
good: good1 good2 good3 good4 good5 good6 good7 good8
	@echo "----------- BAD FORMAT -------------"

good1:
	./main.php "5 * X^0 + 4 * X^1 = 4 * X^0"

good2:
	./main.php "5 * x^0 - 4 * x^1 = 4 * x^0"

good3:
	./main.php "-3.5 + 4 * 5x + 3x^2 = 5x + 3 * 2 - 1"

good4:
	./main.php "-3.5 + 4 * -5.5x + 3x^2 = 5x + 3 * 2 - 1 + -x"

good5:
	./main.php "-4x + 8x - 3x^2 + 3x * 2 = 7x - 8 + 4"

good6:
	./main.php "3x + 7x^2 / 2x + 3 = 4x^2"

good7:
	./main.php "x - 3 + 42x + 8 = 33"

good8:
	./main.php "x - 3 + 42x + 8 = 0"


# 
# UNIT TEST BAD
# 
bad: bad1 bad2 bad3

bad1:
	./main.php "5 + 4 X = 4"

bad2:
	./main.php "5 * X^0 + 3 * X2 + 4 * X^3 = 4 * X^0"

bad3:
	./main.php "3 + 4 x 5x + 3x^2 = 5x + 3 * 2 - 1"