<?php

require_once __DIR__. '/Calcule.class.php';

class Computor {

	private $calc;
	private $result;
	private $calc_str;
	private $result_str;
	private $degreeMax = 0;
	private $degreeMin = 0;

	public function __construct($calcule) {
		list($calc, $result) = split('=', $calcule);
		$this->calc_str = trim($calc);
		$this->result_str = trim($result);
	}

	public function reduce()
	{
		$calc = new Calcule($this->calc_str);
		$result = new Calcule($this->result_str);

		echo $calc .' = '. $result . PHP_EOL;
		while ($result->getN() > 0) {
			$first = $result->getFirst();
			$tmp = $result->mult($first, new Operation(-1, 0));
			$calc->insert($tmp);
		}
		echo $calc .' = 0' . PHP_EOL;
		$this->checkDegreeMax($calc);
		$this->checkDegreeMin($calc);
		$this->calc = $calc;
		$this->result = $result;
	}


	public function resolve()
	{
		switch ($this->degreeMax) {
			case 1:
				$this->resolveP1();
			break;

			case 2:
				$this->resolveP2();
			break;

			default:
				$this->resolveP0();
			break;
		}
	}

	public function hasBadFormat() {
		return (!$this->checkFormat($this->calc_str) 
			|| !$this->checkFormat($this->result_str));
	}

	public function getDegreeMax() {
		return ($this->degreeMax);
	}

	public function getDegreeMin() {
		return ($this->degreeMin);
	}

	private function checkFormat($str) {
		$regNum = '-?(\d+(\.\d+)?[Xx]?|-?(\d+)?[Xx](\^\d+)?)';
		$regOp = '[\/\*+-]';
		$reggex = '/^('. $regNum .' '. $regOp .' )*'. $regNum .'$/';
		if ($str === false || !preg_match($reggex, $str))
			return false;
		return true;
	}

	private function resolveP0() {
		if ($this->calc->getFirst()->getN() == 0) {
			echo 'All real number are the solution' . PHP_EOL;
		} else {
			echo 'There is no solution' . PHP_EOL;
		}
	}

	private function resolveP1() {
		$n1 = $this->calc->getFirst();
		if ($this->calc->getN() == 1) {
			echo "The solution: x = 0". PHP_EOL;
		} else {
			$n2 = $this->calc->getFirst();
			$tmp = $n1;
			if ($n1->getP() != 1) {
				$n1 = $n2;
				$n2 = $tmp;
			}
			echo $n1 .' = '. $this->calc->mult(new Operation(-1, 0), $n2). PHP_EOL;
			echo "The solution: x = ". ($n2->getN() * -1 / $n1->getN()). PHP_EOL;
		}
	}

	private function resolveP2() {
		list($a, $b, $c) = $this->getABC();
		$delta = $this->getDelta($a, $b, $c);

		echo "Discriminant : " . $delta . PHP_EOL;
		echo "a = ". $a . PHP_EOL;
		echo "b = ". $b . PHP_EOL;
		echo "c = ". $c . PHP_EOL;
		if ($delta < 0) {
			echo "This equation has no solution in R" . PHP_EOL;
			echo "The solutions is :" . PHP_EOL;
			echo 'x1 = ('. -$b .' - i√'. $delta.') / (2 * '. $a .')'. PHP_EOL;
			echo 'x2 = ('. -$b .' + i√'. $delta.') / (2 * '. $a .')'. PHP_EOL;
		} else if ($delta > 0) {
			echo "This equation has two solution : ". PHP_EOL;
			echo "x1 = ". (-$b - sqrt($delta)) / (2 * $a). PHP_EOL;
			echo "x2 = ". (-$b + sqrt($delta)) / (2 * $a). PHP_EOL;
		} else {
			echo "This solution : x = ". (-$b / (2 * $a)) . PHP_EOL;
		}
	}

	private function getDelta($a, $b, $c) {
		return (pow($b, 2)) - (4 * $a * $c);
	}

	private function getABC() {
		$n1 = $this->calc->getFirst();
		$n2 = $this->calc->getN() >= 1 ? $this->calc->getFirst() : NULL;
		$n3 = $this->calc->getN() >= 1 ? $this->calc->getFirst() : NULL;

		$a = $this->getA($n1, $n2, $n3)->getN();
		$b = $this->getB($n1, $n2, $n3)->getN();
		$c = $this->getC($n1, $n2, $n3)->getN();
		return [$a, $b, $c];
	}

	private function getA($n1, $n2, $n3) {
		if ($n1 && $n1->getP() == 2) {
			return $n1;
		} else if ($n2 && $n2->getP() == 2) {
			return $n2;
		} else if ($n3 && $n3->getP() == 2) {
			return $n3;
		} else {
			return new Operation(0, 2);
		}
	}

	private function getB($n1, $n2, $n3) {
		if ($n1 && $n1->getP() == 1) {
			return $n1;
		} else if ($n2 && $n2->getP() == 1) {
			return $n2;
		} else if ($n3 && $n3->getP() == 1) {
			return $n3;
		} else {
			return new Operation(0, 1);
		}
	}

	private function getC($n1, $n2, $n3) {
		if ($n1 && $n1->getP() == 0) {
			return $n1;
		} else if ($n2 && $n2->getP() == 0) {
			return $n2;
		} else if ($n3 && $n3->getP() == 0) {
			return $n3;
		} else {
			return new Operation(0, 0);
		}
	}

	private function checkDegreeMax($calc) {
		$this->degreeMax = $calc->getMaxPower();
	}

	private function checkDegreeMin($calc) {
		$this->degreeMin = $calc->getMinPower();
	}

}
