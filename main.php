#!/usr/bin/php
<?php

require_once(__DIR__ .'/computor.class.php');

if ($argc != 2) {
	echo "usage: ./main.php <equation>". PHP_EOL;
	return (1);
}
$computor = new Computor($argv[1]);

if ($computor->hasBadFormat()) {
	echo "Bad format" . PHP_EOL;
	return ;
}
$computor->reduce();
$degreeMax = $computor->getDegreeMax();
$degreeMin = $computor->getDegreeMin();
if ($degreeMax > 2) {
	echo "The polynomial degree is stricly greater than 2, I can't solve." . PHP_EOL;
	return ;
}
if ($degreeMin < 0) {
	echo "I can't solve because you have a negative power." . PHP_EOL;
	return ;
}

echo "Polynomial degree: ". $degreeMax . PHP_EOL;
$computor->resolve();